#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/ref.hpp>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )

		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
        fprintf(file, get_line());  // leci wyjatek

    fclose(file);
}

class FileHandle
{
    FILE* file_;
    FileHandle(const FileHandle&);
    FileHandle& operator=(const FileHandle&);
public:
    FileHandle(FILE* file) : file_(file)
    {
    }

    ~FileHandle()
    {
        fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }

    operator bool() const
    {
        return file_ != NULL;
    }
};

// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileHandle file(fopen(file_name, "w"));

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku");


    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());  // leci wyjatek
}

void save_to_file_with_shared_ptr(const char* file_name)
{
    boost::shared_ptr<FILE> file(fopen(file_name, "w"), &fclose);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku");


    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());  // leci wyjatek
}

class Test
{
    std::string str_;
public:
    Test(std::string& str) : str_(str)
    {}

    void print() const
    {
        std::cout << str_ << std::endl;
    }
};

int main()
try
{
    std::string txt = "Test";
    boost::shared_ptr<Test> ptr_t1 = boost::make_shared<Test>(boost::ref(txt));

    ptr_t1->print();

    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    save_to_file_with_shared_ptr("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::string temp;
    std::getline(std::cin, temp);
}
