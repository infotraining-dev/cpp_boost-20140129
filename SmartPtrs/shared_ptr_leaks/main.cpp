#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

class Human
{
public:
	Human()
	{
		std::cout << "Konstruktor Human()\n";
	}
	
	~Human()
	{
		std::cout << "Desktruktor ~Human()\n";
	}
	
	void SetPartner(boost::shared_ptr<Human> partner)
	{
		partner_ = partner;
	}
private:
	boost::shared_ptr<Human> partner_;
};

void memoryLeakDemo()
{
	// RC husband == 1
	boost::shared_ptr<Human> husband(new Human);
	
	// RC wife == 1
	boost::shared_ptr<Human> wife(new Human);
	
    // RC wife == 2
	husband->SetPartner(wife);
	
	// RC husband == 2
	wife->SetPartner(husband);
}

int main()
{
	memoryLeakDemo();
}
