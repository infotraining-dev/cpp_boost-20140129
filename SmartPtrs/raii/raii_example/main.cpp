#include <iostream>

using namespace std;

class Mutex
{
public:
    void lock()
    {
        cout << "Mutex locked!" << endl;
    }

    void unlock()
    {
        cout << "Mutex unlocked!" << endl;
    }
};

class LockGuard
{
    Mutex& mtx_;
    LockGuard(const LockGuard&);
    LockGuard& operator=(const LockGuard&);
public:
    LockGuard(Mutex& mtx) : mtx_(mtx)
    {
        mtx_.lock();
    }

    ~LockGuard()
    {
        mtx_.unlock();
    }
};

void may_throw()
{
    throw 13;
}

class SynchonizedObject
{
    Mutex mtx_;
public:

    void run()
    {
        LockGuard lk(mtx_);

        cout << "Run..." << endl;

        may_throw();
    }
};

int main() try
{
    SynchonizedObject so;

    so.run();

    return 0;
}
catch(...)
{
    cout << "End of program" << endl;
}

