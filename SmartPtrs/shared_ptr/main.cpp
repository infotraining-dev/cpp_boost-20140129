#include <iostream>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <cassert>
#include <cstdlib>
#include <stdexcept>

class X
{
public:
    // konstruktor
    X(int value = 0)
        : value_(value)
    {
        std::cout << "Konstruktor X(" << value_ << ")\n";
    }

    // destruktor
    ~X()
    {
        std::cout << "Destruktor ~X(" << value_ << ")\n";
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
    int value_;
};


class A
{
	boost::shared_ptr<X> x_;
public:
	A(boost::shared_ptr<X> x) : x_(x) {}
	
	int get_value()
	{
		return x_->value();
	}
};

class B
{
	boost::shared_ptr<X> x_;
public:
	B(boost::shared_ptr<X> x) : x_(x) {}
	
	void set_value(int i)
	{
        x_->set_value(i);
	}	
};

class Base
{
public:
    virtual void print() const
    {
        std::cout << "Base::print()" << std::endl;
    }

    virtual ~Base() {}
};

class Derived : public Base
{
public:
    virtual void print() const
    {
        std::cout << "Derived::print()" << std::endl;
    }

    void derived_only()
    {
        std::cout << "Derived::derived_only()" << std::endl;
    }
};


int main()
{
    boost::shared_ptr<Base> pbase(new Base());

    pbase->print();

    boost::shared_ptr<Derived> pderived = boost::dynamic_pointer_cast<Derived>(pbase);

    if (pderived)
        pderived->derived_only();

	{
		boost::shared_ptr<X> spX1(new X());
		std::cout << "RC: " << spX1.use_count() << std::endl;
		{
            boost::shared_ptr<X> spX2(spX1);
			std::cout << "RC: " << spX2.use_count() << std::endl;
		}
		std::cout << "RC: " << spX1.use_count() << std::endl;
	}

	std::cout << "\n//----------------------\n\n";

	boost::shared_ptr<X> temp( new X(14) );
	A a(temp);
	{
		B b(temp);
		b.set_value(28);
	}
	
	assert(a.get_value() == 28);
	std::cout << "a.get_value() = " << a.get_value() << std::endl;
}
