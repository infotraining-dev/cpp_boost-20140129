#include <memory>
#include <iostream>
#include <exception>
#include <stdexcept>

using namespace std;

class X
{
public:
	// konstruktor
	X(int value = 0)
		: value_(value)
	{
		std::cout << "Konstruktor X(" << value_ << ")\n"; 
	}
	
	// destruktor
	~X() 
	{
		std::cout << "Destruktor ~X(" << value_ << ")\n"; 
	}
	
    int value() const
	{
		return value_;
	}

    void set_value(int value)
	{
        value_ = value;
	}

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
	int value_;
};

void legacy_code(X* ptr)
{
    ptr->set_value(5);
    cout << "legacy_code: new value = " << ptr->value() << endl;
}

X* factory(int arg) // TODO: poprawa z wykorzystaniem auto_ptr
{
    return new X(arg);
}

void unsafe1()  // TODO: poprawa z wykorzystaniem auto_ptr
{
    X* ptrX = factory(4);

	/* kod korzystajacy z ptrX */

    legacy_code(ptrX);

    ptrX->unsafe();

	delete ptrX;
}

int main() try
{
    unsafe1();
}
catch(...)
{
    std::cout << "Zlapalem wyjatek!" << std::endl;
}
