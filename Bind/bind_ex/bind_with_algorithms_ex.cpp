#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <boost/bind.hpp>

using namespace std;

int main()
{
	vector<Person> employees;
	fill_person_container(employees);

	cout << "Wszyscy pracownicy:\n";
	copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
	cout << endl;

	// wyswietl pracownikow z pensja powyzej 3000
	cout << "\nPracownicy z pensja powy�ej 3000:\n";
    remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
                   !(boost::bind(&Person::salary, _1) > 3000.0));


	// wy�wietl pracownik�w o wieku poni�ej 30 lat
	cout << "\nPracownicy o wieku poni�ej 30 lat:\n";
    remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
                   !(boost::bind(&Person::age, _1) < 30));
	
	// posortuj malej�co pracownikow wg nazwiska
	cout << "\nLista pracownik�w wg nazwiska (malejaco):\n";
    sort(employees.begin(), employees.end(),
         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
	
	// wy�wietl kobiety
	cout << "\nKobiety:\n";
    remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
                   !(boost::bind(&Person::gender, _1) == Female));
	
	// ilosc osob zarabiajacych powyzej sredniej
	cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
    double avg = accumulate(employees.begin(), employees.end(), 0.0,
                            bind(plus<double>(), _1, bind(&Person::salary, _2))) / employees.size();

    // ilosc osob zarabiajacych powyzej sredniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej: " <<
            count_if(employees.begin(), employees.end(),
                     bind(&Person::salary, _1) > avg)  << endl;
}
