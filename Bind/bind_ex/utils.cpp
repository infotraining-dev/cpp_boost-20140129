#include "utils.hpp"

int Tracer::counter_ = 0;
bool Tracer::verbose_ = false;

Tracer::Tracer() : id_(++counter_), copy_constr_(0), assign_op_(0)
{
	if (verbose_)
		std::cout << "Tracer(" << id_ << ")\n";
}

Tracer::~Tracer()
{
	if (verbose_)
		std::cout << "~Tracer(" << id_ << ")\n";
}

Tracer::Tracer(const Tracer& source) : id_(source.id_), copy_constr_(++source.copy_constr_), assign_op_(source.assign_op_)
{
	if (verbose_)
		std::cout << "Tracer(const Tracer& t - " << id_ << ")\n";
}

Tracer& Tracer::operator=(const Tracer& source)
{
	id_ = source.id_;
	copy_constr_ = source.copy_constr_;
	assign_op_ = ++source.assign_op_;

	if (verbose_)
		std::cout << "operator =(const Tracer& t - " << id_ << ")\n";

	return *this;
}

int Tracer::id() const
{
	return id_;
}

int Tracer::copy_constr() const
{
	return copy_constr_;
}

int Tracer::assign_op() const
{
	return assign_op_;
}

void Tracer::set_verbose(bool verbose_flag)
{
	verbose_ = verbose_flag;
}

std::ostream& operator <<(std::ostream& out, const Tracer& t)
{
	out << "Tracer(id = " << t.id() << ", copy_constr = " << t.copy_constr() 
		<< ", assign_op = " << t.assign_op() << ")"; 

	return out;
}