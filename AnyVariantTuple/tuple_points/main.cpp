#include <iostream>
#include <cmath>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/static_assert.hpp>
#include <boost/fusion/container.hpp>
#include <boost/fusion/view.hpp>
#include <boost/fusion/algorithm.hpp>
#include <boost/fusion/adapted/boost_tuple.hpp>
#include <boost/fusion/adapted/array.hpp>
#include <boost/fusion/include/array.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

using namespace std;

template <typename P1, typename P2, int Index>
struct Pythagoras
{
public:
    static double calc(const P1& p1, const P2& p2)
    {
        double d = boost::get<Index-1>(p1) - boost::get<Index-1>(p2);

        return d * d + Pythagoras<P1, P2, Index-1>::calc(p1, p2);
    }
};

template <typename P1, typename P2>
struct Pythagoras<P1, P2, 0>
{
    static double calc(const P1& p1, const P2& p2)
    {
        return 0;
    }
};

template <typename P1, typename P2>
double pt_distance(const P1& p1, const P2& p2)
{
    BOOST_STATIC_ASSERT(
        boost::tuples::length<P1>::value ==
                boost::tuples::length<P2>::value);

    double accumulated
            = Pythagoras<P1, P2,
                boost::tuples::length<P1>::value>::calc(p1, p2);

    return sqrt(accumulated);
}


struct PythagorasFusion
{
    typedef double result_type;

    template <typename T>
    double operator()(double acc, const T& axis) const
    {
        double d = boost::fusion::at_c<0>(axis) - boost::fusion::at_c<1>(axis);

        return acc + d * d;
    }
};

// distance with fusion
template <typename P1, typename P2>
double pt_distance_with_fusion(P1 p1, P2 p2)
{
    typedef boost::fusion::vector<P1&, P2&> ZipType;

    double accumulated = boost::fusion::fold(
                boost::fusion::zip_view<ZipType>(ZipType(p1, p2)),
                0, PythagorasFusion());

    return sqrt(accumulated);
}

struct Coord
{
    double x;
    double y;
};

BOOST_FUSION_ADAPT_STRUCT
(
   Coord,
   (double, x)
   (double, y)
)


int main()
{
    typedef boost::tuple<double, double> Point2D;
    typedef boost::tuple<double, double, double> Point3D;

    Point2D pt1(1.0, 2.0);
    Point2D pt2(3.0, 2.0);

    cout << "distance between " << pt1 << " and " << pt2
         << " = " << pt_distance(pt1, pt2) << endl;


    cout << "fusion distance between " << pt1 << " and " << pt2
         << " = " << pt_distance_with_fusion(pt1, pt2) << endl;

    Coord c1 = { 0.0, 0.0 };
    Coord c2 = { 1.0, 1.0 };

    cout << "fusion distance between coords "
         << " = " << pt_distance_with_fusion(c1, c2) << endl;

}

