#include <iostream>
#include <string>
#include <boost/any.hpp>

using namespace std;

struct Person
{
    int id;
    string name;

    Person(int id, const string& name) : id(id), name(name)
    {}
};

int main()
{
    boost::any a1;

    cout << "is empty: " << a1.empty() << endl;

    a1 = 5;

    double dx = 3.14;

    a1 = dx;

    cout << "a1 type: " << a1.type().name() << endl;

    a1 = string("Text");

    cout << "a1 type: " << a1.type().name() << endl;

    a1 = Person(1, "Kowalski");

    cout << "a1 type: " << a1.type().name() << endl;

    Person p1 = boost::any_cast<Person>(a1);

    cout << p1.id << " " << p1.name << endl;

    try
    {
        string str = boost::any_cast<string>(a1);
    }
    catch(const boost::bad_any_cast& e)
    {
        cout << e.what() << endl;
    }

    Person* ptr_p1 = boost::unsafe_any_cast<Person>(&a1);

    if (ptr_p1)
    {
        cout << ptr_p1->id << " " << ptr_p1->name << endl;
    }
}

