#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>

using namespace std;


template <typename Tuple, int Index>
class ElementAt
{
public:
    static void print(const Tuple& t)
    {
        ElementAt<Tuple, Index-1>::print(t);
        cout << "Item " << Index << " - " << boost::get<Index>(t) << endl;
    }
};

template <typename Tuple>
class ElementAt<Tuple, 0>
{
public:
    static void print(const Tuple& t)
    {
        cout << "Item " << 0 << " - " << boost::get<0>(t) << endl;
    }
};

template <typename Tuple>
void print(const Tuple& t)
{
    ElementAt<Tuple, boost::tuples::length<Tuple>::value - 1>::print(t);
}

// alternatywna implementacja - head + tail
template <typename Tuple>
void print_tuple(const Tuple& t)
{
    cout << "Item - " << t.get_head() << endl;

    print_tuple(t.get_tail());
}

template <>
void print_tuple<boost::tuples::null_type>(const boost::tuples::null_type&)
{
}

// for_each
template <typename Tuple, typename Func1>
void for_each(const Tuple& t, Func1 func)
{
    func(t.get_head());

    for_each(t.get_tail(), func);
}

template <typename Func>
void for_each(const boost::tuples::null_type&, Func func)
{
}

class Printer
{
public:
    void operator()(int val) const
    {
        cout << "int: " << val << endl;
    }

    void operator()(double val) const
    {
        cout << "double: " << val << endl;
    }

    void operator()(string val) const
    {
        cout << "string: " << val << endl;
    }
};

int main()
{
    typedef boost::tuple<int, double, string> TupleIDS;

    TupleIDS t1(1, 3.14, "Text");

    print(t1);

    cout << "\n\n";

    print_tuple(t1);

    cout << "\n\n";

    for_each(t1, Printer());
}

