#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <complex>
#include <boost/assign.hpp>
#include <boost/variant.hpp>
#include <boost/mpl/for_each.hpp>


using namespace std;

// recursive types with recursive_wrapper

struct Add;
struct Sub;
template <typename OpTag> struct BinaryOp;

typedef boost::variant<
	int,
	boost::recursive_wrapper<BinaryOp<Add> >,
	boost::recursive_wrapper<BinaryOp<Sub> >
> Expression;

template <typename OpTag>
struct BinaryOp
{
	Expression left;
	Expression right;

	BinaryOp(const Expression& lhs, const Expression& rhs) : left(lhs), right(rhs) {}
};

class Calculator : public boost::static_visitor<int>
{
public:
	int operator()(int value) const
	{
		return value;
	}

	int operator()(const BinaryOp<Add>& binary) const
	{
		return boost::apply_visitor(Calculator(), binary.left) + boost::apply_visitor(Calculator(), binary.right);
	}

	int operator()(const BinaryOp<Sub>& binary) const
	{
		return boost::apply_visitor(Calculator(), binary.left) - boost::apply_visitor(Calculator(), binary.right);
	}
};

void do_math()
{
	Expression math_expression(BinaryOp<Add>(BinaryOp<Sub>(7, 3), 8));  // ((7-3)+8)

	cout << "(7-3) + 8 = " << boost::apply_visitor(Calculator(), math_expression) << endl;
}

// recursive types with boost::make_recursive_variant
typedef boost::make_recursive_variant<string, vector<boost::recursive_variant_> >::type StringTreeType;

StringTreeType build_tree()
{
	using namespace boost::assign;

    vector<StringTreeType> subresult;
	subresult += "three", "four";

    vector<StringTreeType> result;
	result += "one", "two", subresult, "five", "six";

	return result;
}

class PrintTree : public boost::static_visitor<>
{
public:
	void operator()(const string& item) const
	{
		cout << item << " ";
	}

    void operator()(const vector<StringTreeType>& vec) const
	{
		cout << "( ";
        PrintTree printer;
        for_each(vec.begin(), vec.end(), boost::apply_visitor(printer));
		cout << ") ";
	}
};

int main()
{
	do_math();

	cout << "\n--------------------------\n";

    StringTreeType string_tree = build_tree();

	boost::apply_visitor(PrintTree(), string_tree);
	cout << endl;
}
