#include <iostream>
#include <string>
#include <complex>
#include <boost/variant.hpp>
#include <boost/mpl/for_each.hpp>

using namespace std;

struct Print
{
	template <typename T>
	void operator()(T& t) const
	{
		cout << typeid(t).name() << endl;
	}
};

int main()
{
	typedef boost::variant<int, double, string> VariantBasic;
	VariantBasic var1 = "Text";

	cout << "var1 is " << (var1.empty() ? "empty" : "not empty") << endl;
	cout << "Type: " << var1.type().name() << "; Value: " << var1 << endl;

	string& str = boost::get<string>(var1);
	str += " extended";

	cout << "var1.which() = " << var1.which() << endl;

	cout << "\n-----------------------------\n";

	// list of types extension in boost::variant

	typedef VariantBasic::types ListOfTypes;
	typedef boost::mpl::push_front<ListOfTypes, complex<double> >::type ExtendedListOfTypes;

	typedef boost::make_variant_over<ExtendedListOfTypes>::type VariantExtended;
	VariantExtended var2;

	var2 = complex<double>(1.0, 1.0);
	cout << var2.type().name() << endl;

	cout << "\nList of types for VariantExtended:\n";
	boost::mpl::for_each<VariantExtended::types>(Print());
}
