#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <vector>
#include <algorithm>
#include <iterator>
#include <boost/assign.hpp>

using namespace std;
using namespace boost::assign;

void foo(int x, const string& str, double dx)
{
    cout << "x = " << x << " str = " << str
         << " dx = " << dx << endl;
}

class Add  : public binary_function<int, int, int>
{
public:
    //typedef int result_type;

    int operator()(int x, int y)
    {
        return x + y;
    }
};

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& n) : id_(id), name_(n)
    {
    }

    void print(const string& prefix)
    {
        cout << prefix << id_ << " " << name_ << endl;
    }
};

int main()
{
    auto bound_f_c11 = boost::bind(&foo, _1, "Text", _2);  // c++11

    boost::function<void (int, double)> bound_f = bound_f_c11;

    bound_f(1, 10);

    auto f_0 = boost::bind(&foo, 1, "Text", 3.14);

    f_0();

    auto add_4 = boost::bind<int>(Add(), _1, 4);

    cout << "5 + 4 = " << add_4(5) << endl;

    vector<int> numbers;
    numbers += 1, 2, 3, 4, 5, 6;

    transform(numbers.begin(), numbers.end(), numbers.begin(),
              boost::bind(plus<int>(), _1, 4));

    copy(numbers.begin(), numbers.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    Person p(1, "Kowalski");

    auto bound_print = boost::bind(&Person::print, &p, "Person: ");

    bound_print();

    auto lazy_bound_print = boost::bind(&Person::print, _1, "Person: ");

    lazy_bound_print(p);

    vector<Person> people;
    people += Person(2, "Nowak"), Person(3, "Nijaki");

    for_each(people.begin(), people.end(), boost::bind(&Person::print, _1, "Osoba: "));
}

