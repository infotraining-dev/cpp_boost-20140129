#include <iostream>
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_pointer.hpp>

using namespace std;

template <typename T, size_t i>
void accepts_arrays_with_size_over_1024(T (&arr)[i])
{
    BOOST_STATIC_ASSERT((i > 1024) && (sizeof(T) >= 2) && boost::is_integral<T>::value);

    cout << "accepts_arrays_with_size_over_1024" << endl;
}

template <typename T>
struct IsSmartPtr : public boost::false_type
{
};

template <typename T>
struct IsSmartPtr<boost::shared_ptr<T> > : public boost::true_type
{
};

template <typename T>
struct IsSmartPtr<boost::scoped_ptr<T> > : public boost::true_type
{
};

template <typename T>
struct IsSmartPtr<boost::intrusive_ptr<T> > : public boost::true_type
{
};

template <typename T>
void dereference_pointer(const T& ptr)
{
    BOOST_STATIC_ASSERT(boost::is_pointer<T>::value || IsSmartPtr<T>::value);

    cout << *ptr << endl;
}

int main()
{
    int tab1[2048];

    accepts_arrays_with_size_over_1024(tab1);

    boost::shared_ptr<int> ptr1(new int(1));
    boost::scoped_ptr<int> ptr2(new int(2));

    dereference_pointer(ptr1);
    dereference_pointer(ptr2);

    int x = 10;

    dereference_pointer(&x);
}

