#include "Deleter.hpp"
#include <boost/checked_delete.hpp>

void Deleter::delete_it(ToBeDeleted* p)
{
    //delete p;
    boost::checked_delete(p);


    std::vector<int*> vec_pointers;

    vec_pointers.push_back(new int(1));
    vec_pointers.push_back(new int(2));

    std::for_each(vec_pointers.begin(), vec_pointers.end(),
                  &boost::checked_delete<int>());
}
