#include "ToBeDeleted.hpp"
#include "Deleter.hpp"

//class SomeClass;

//SomeClass* create()
//{
//	return (SomeClass*)0;
//}

//void simple_test()
//{
//    SomeClass* p = create();

//    delete p; // w najlepszym razie warning
//}

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
	real_test();
}
