#include <iostream>
#include <string>
#include <cstring>
#include <boost/type_traits/is_arithmetic.hpp>
#include <boost/type_traits/is_pod.hpp>
#include <boost/type_traits/is_float.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/mpl/or.hpp>
#include <boost/assign.hpp>
#include <algorithm>
#include <iterator>

using namespace std;

void foo(int n)
{
    cout << "foo(int)" << endl;
}

//template <typename T>
//void foo(T obj, typename T::nested_type* ptr = 0)
//{
//    cout << "generic foo(T)" << endl;
//}

//struct MyType
//{
//    int value;

//    typedef void nested_type;
//};

template <typename T>
void foo(T obj,
         typename boost::disable_if<
            boost::mpl::or_<
                boost::is_integral<T>,
                boost::is_float<T> > >::type* ptr = 0)
{
    cout << "foo(T) for non-arithmetic and non-pointers " << endl;
}

template <typename InIt, typename OutIt>
void mcopy(InIt begin, InIt end, OutIt dest)
{
    cout << "Generic mcopy" << endl;

    while(begin != end)
    {
        *(dest++) = *(begin++);
    }
}

template <typename T>
void mcopy(T* begin, T* end, T* dest,
           typename boost::enable_if<boost::is_pod<T> >::type* ptr = 0)
{
    cout << "Optimized mcopy" << endl;

    memcpy(dest, begin, (end-begin)*sizeof(T));
}

template <typename InIt>
typename iterator_traits<InIt>::value_type sum(InIt begin, InIt end)
{
    typedef typename iterator_traits<InIt>::value_type ValueType;

    ValueType sum = ValueType();

    while (begin != end)
        sum += *(begin++);

    return sum;
}

template <typename T, typename Enable = void>
class DataProcessor
{
public:
    void process_data(T* arr, size_t size)
    {
        cout << "Generic data processor" << endl;
    }
};

template <typename T>
class DataProcessor<T, typename boost::enable_if<boost::is_float<T> >::type>
{

public:
    void process_data(T* arr, size_t size)
    {
        cout << "Optimized data processor" << endl;
    }
};



int main()
{
    using namespace boost::assign;

    vector<int> vecint;
    vecint += 1, 2, 3, 4;

    vector<int>::iterator it = vecint.begin();
    advance(it, 2);

    foo(4);

    int x = 10;

    foo(x);

    short sx = 25;

    foo(sx);

    double dx = 0.25;

    foo(dx);

//    MyType mt { 1 };

//    foo(mt);

    string str = "sfinae";

    foo(str);

    string words1[] = { "Ala", "ma", "kota" };
    string words2[3];

    mcopy(words1, words1+3, words2);  // generic impl

    mcopy(words2, words2+3, ostream_iterator<string>(cout, " "));
    cout << "\n";

    int numbers1[] = { 1, 2, 3 };
    int numbers2[3];

    int result = sum(numbers1, numbers1+3);

    cout << "sum: " << result << endl;

    mcopy(numbers1, numbers1 + 3, numbers2); // optimized version

    mcopy(numbers2, numbers2+3, ostream_iterator<int>(cout, " "));
    cout << "\n";

    DataProcessor<int> dp_int;

    dp_int.process_data(numbers1, 3);

    double data[] = { 1.0, 2.0, 3.0 };

    DataProcessor<double> dp_double;

    dp_double.process_data(data, 3);
}

