#include <iostream>
#include <string>
#include <boost/utility/base_from_member.hpp>

class Logger
{
public:
	virtual void log(const std::string& message) = 0;
	virtual ~Logger() {}
};

class ConsoleLogger : public Logger
{
public:
	ConsoleLogger()
	{
		std::cout << "ConsoleLogger ctor\n";
	}

	void log(const std::string& message)
	{
		std::cout << "Log: " << message << std::endl;
	}
};

class FileLogger : public Logger
{
public:
    FileLogger()
    {
        std::cout << "FileLogger ctor\n";
    }

    void log(const std::string& message)
    {
        std::cout << "Log to file: " << message << std::endl;
    }
};

class Service
{
    Logger* logger1_;
    Logger* logger2_;
public:
    Service(Logger* logger1, Logger* logger2) : logger1_(logger1), logger2_(logger2)
	{
        logger1_->log("Service ctor");
        logger2_->log("Service ctor");
	}

	void run()
	{
        logger1_->log("Start of work...");
        logger2_->log("Start of work...");
		do_work();
        logger1_->log("End of work...");
        logger2_->log("Start of work...");
	}
protected:
	virtual void do_work() = 0;
};

//namespace Before
//{
//	class MyService : public Service
//	{
//		ConsoleLogger logger_;
//	public:
//		MyService() : Service(&logger_)
//		{

//		}
//	protected:
//		void do_work()
//		{
//			std::cout << "Service is running..." << std::endl;
//		}
//	};
//}

namespace After
{
    class MyService
            : protected boost::base_from_member<ConsoleLogger, 1>,
              protected boost::base_from_member<ConsoleLogger, 2>,
                      public Service
	{
        typedef boost::base_from_member<ConsoleLogger, 1> Logger1;
        typedef boost::base_from_member<ConsoleLogger, 2> Logger2;

	public:
        MyService() : Service(&(this->Logger1::member), &(this->Logger2::member))
		{}
	protected:
		void do_work()
		{
			std::cout << "Service is running..." << std::endl;
		}
	};
}


int main()
{
//    Before::MyService srv1;
//    srv1.run();


	std::cout << "\n-----------------------------------\n";

	After::MyService srv2;
	srv2.run();
}
