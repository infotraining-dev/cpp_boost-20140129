#include <iostream>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

void my_slot(int arg)
{
    cout << "my_slot() - "<< arg << endl;
}

class MySlot
{
public:
    void operator()(int arg)
    {
        cout << "MySlot: " << arg << endl;
    }
};

class Printer
{
public:
    void print(const string& msg, double arg)
    {
        cout << msg << arg << endl;
    }
};

class Logger
{
public:
    void log(const string& msg)
    {
        cout << "Log: " << msg << endl;
    }
};

void save_to_db(double temp)
{
    cout << "save to db: " << temp << endl;
}

class TemperatureMonitor
{
public:
    typedef boost::signals2::signal<void (double)> TemperatureChangedSignal;
    typedef TemperatureChangedSignal::slot_type TemperatureChangedSlot;

    TemperatureMonitor() : temp_(0.0) {}

    boost::signals2::connection attach(TemperatureChangedSlot slot)
    {
        return temp_changed_.connect(slot);
    }

    void set_temp(double temp)
    {
        if (temp_ != temp)
        {
            temp_ = temp;
            temp_changed_(temp_);  // emisja sygnalu
        }
    }

private:
    TemperatureChangedSignal temp_changed_;
    double temp_;
};
int main()
{
    using namespace boost::signals2;

    Printer printer;

    typedef signal<void(int)> Signal;
    typedef Signal::slot_type SlotType;

    Signal sig1;

    sig1.connect(2, &my_slot);

    SlotType slot = MySlot();
    sig1.connect(0, slot);
    sig1.connect(0, &my_slot, at_front);
    connection conn_to_printer = sig1.connect(1, boost::bind(&Printer::print, &printer, "Printing ", _1));

    sig1(100);  // emisja sygnalu

    cout << "\n\n";

    sig1.disconnect(&my_slot);
    conn_to_printer.disconnect();

    sig1(200);

    cout << "\n\n";

    TemperatureMonitor monitor;
    Logger logger;

    monitor.attach(boost::bind(&Printer::print, &printer, "Printing new temp: ", _1));
    connection conn2log = monitor.attach(boost::bind(&Logger::log, &logger, boost::bind(&boost::lexical_cast<string, double>, _1)));

    monitor.set_temp(24.0);
    monitor.set_temp(45.0);

    {
        shared_connection_block block(conn2log);

        scoped_connection scoped_conn(monitor.attach(&save_to_db));

        monitor.set_temp(124.0);
        monitor.set_temp(245.0);
    }

    monitor.set_temp(45.0);

    {
        boost::shared_ptr<Printer> ptr_printer(new Printer);

        monitor.attach(TemperatureMonitor::TemperatureChangedSlot(
                           &Printer::print, ptr_printer.get(), "Shared printer: ", _1).track(ptr_printer));

        monitor.set_temp(400.0);
    }

     monitor.set_temp(45.0);
}
